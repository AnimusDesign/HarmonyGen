package harmonygenapi

import harmonygenapi.Routes.MusicBrainz.musicBrainzAPI
import harmonygenapi.Routes.harmonygen.buildSpotifyURL
import harmonygenapi.Routes.harmonygen.harmonyGenAPI
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.gson.gson
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.prometheus.client.exporter.HTTPServer
import org.slf4j.LoggerFactory
import java.text.DateFormat
import java.util.*

fun loadAppProperties(props: String = "/harmonygenapi.properties"): Properties {
    logger.debug("Loading Application Properties")
    val properties = Properties()
    properties.load(Properties::class.java.getResourceAsStream(props))
    return properties
}

/**
 * Main logic to build the KTor routing paths.
 */
fun Application.module() {
    val properties = loadAppProperties()
    val musicBrainzConn = musicBrainzContext()
    val harmonyGenConn = harmonyGenContext()
    install(DefaultHeaders)
    install(CallLogging)
    install(ContentNegotiation) {
        gson {
            setDateFormat(DateFormat.LONG)
            setPrettyPrinting()
        }
    }
    install(Routing) {
        musicBrainzAPI(musicBrainzConn, properties.getProperty("api.path") as String)
        harmonyGenAPI(harmonyGenConn, properties.getProperty("api.path") as String)
        val childRoutes = this.children
        get(properties.getProperty("api.path")) {
            var resp = arrayListOf<String>()
            call.respond(digestRoutes(childRoutes, resp))
        }
    }

}

/**
 * Helper method that lists all registered routes, and returns as a list.
 *
 * @param routes A list of KTor route objects.
 * @param resp An array list where the route destination will be stored.
 * @return An array list containing the string representation of the route.
 */
fun digestRoutes(routes: List<Route>, resp: ArrayList<String>): ArrayList<String> {
    routes.forEach {
        if (it.children.isNotEmpty()) {
            digestRoutes(it.children, resp)
        } else {
            resp.add(it.toString())
        }

    }
    return resp
}

/**
 * The main director entry function, START HERE!
 *
 * This will read properties from the harmonygenapi.properties file.
 * Loading a base path, and port to listen on. The main api server
 * will listen on the defined port. Metrics will be exportd on the
 * api port + 1. That is to say if your port is configured as 9080,
 * metrics will be exported on 9081
 *
 * @author Animus Null
 * @param args An array of arguments.
 */
fun main(args: Array<String>) {
    logger.info("Welcome to HarmonyGenAPI.")
    val properties = loadAppProperties()
    val port = properties.getProperty("api.port").toInt()
    val spotifyProps = loadAppProperties("/spotify.properties")
    val spotifyState = UUID.randomUUID()
    val url = buildSpotifyURL(
            spotifyProps.getProperty("spotify.clientid"),
            spotifyProps.getProperty("spotify.clientsecret"),
            spotifyProps.getProperty("spotify.scopes"),
            spotifyState
    )
    println("!!!!!!!!!!!!!!!!!!!!!!!!")
    println(url)
    println("!!!!!!!!!!!!!!!!!!!!!!!!")
    Thread({
        embeddedServer(
                Netty,
                port,
                module = Application::module
        ).start()
    }).start()
    logger.info("Started API Server on http://localhost:$port")
    Thread({
        HTTPServer(port + 1)
    }).start()
    logger.info("Started Metric Server on http://localhost:${port + 1}")
}