package harmonygenapi

import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.slf4j.LoggerFactory
import java.sql.DriverManager
import java.util.*
val logger = LoggerFactory.getLogger("harmonygenapi")

/**
 * Helper method to load the Jooq DSL Context.
 *
 * Given a properties file name. Load the properties file.
 * Constructing a DriverManager for the expected SQL database.
 *
 * The expected input for a database connection are as follows.
 *
 * * db.url: The JDBC url for connecting.
 * * db.username: The database username to authenticate with.
 * * db.password The database password to authenticate with.
 *
 * @param properties_file The properties file containing database information to load.
 * @author Animus Null
 */
fun getDBContext(properties_file: String): DSLContext {
    logger.debug("Loading properties from $properties_file")
    val properties = Properties()
    properties.load(Properties::class.java.getResourceAsStream(properties_file))
    val manager = DriverManager.getConnection(
            properties.getProperty("db.url"),
            properties.getProperty("db.username"),
            properties.getProperty("db.password")
    )
    logger.debug("Successfully built SQL Manager, building Jooq DSL")
    return DSL.using(
            manager,
            SQLDialect.POSTGRES
    )
}

fun musicBrainzContext(): DSLContext {
    return getDBContext("/musicbrainz.properties")
}

fun harmonyGenContext(): DSLContext {
    return getDBContext("/harmonygen.properties")
}
