package harmonygenapi

data class RestError(
        val error:String,
        val message:String
)

data class RestResponse(
        val message: String
)