package harmonygenapi.Routes.harmonygen

import animus.design.kharmonygendb.tables.records.SpotifyAuthRecord
import com.wrapper.spotify.Api
import harmonygenapi.RestError
import harmonygenapi.RestResponse
import harmonygenapi.Routes.MusicBrainz.buildMetricObservers
import harmonygenapi.Routes.MusicBrainz.logger
import harmonygenapi.Routes.MusicBrainz.obseverMetrics
import harmonygenapi.loadAppProperties
import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import org.jooq.DSLContext
import java.time.OffsetDateTime
import java.util.*


fun Routing.spotifyCallBack(harmonygen_ctx: DSLContext, apiBaseURL: String = "/api/v1/HarmonyGen") {
    logger.debug("In spotify callback.")
    val path = "$apiBaseURL/SpotifyCallBack"
    val (counter, requestLatency) = buildMetricObservers(path, "HarmonyGenSpotifyCallBack")

    get(path) {
        try {
            val spotifyProps = loadAppProperties("/spotify.properties")
            val requestTimer = requestLatency.startTimer()
            val api = getSpotifyAPI()
            val authorizationCodeCredentials = api.authorizationCodeGrant(call.parameters["code"]).build().get()
            val expiration = Calendar.getInstance()
            val scopes = spotifyProps.getProperty("spotify.scopes").split(" ").toTypedArray()
            expiration.add(Calendar.SECOND, authorizationCodeCredentials.expiresIn)
            api.setAccessToken(authorizationCodeCredentials.accessToken)
            api.setRefreshToken(authorizationCodeCredentials.refreshToken)
            val user = api.me.build().get()
            addSpotifyAccessToken(
                    harmonygen_ctx,
                    user.id.toString(),
                    authorizationCodeCredentials.accessToken,
                    authorizationCodeCredentials.refreshToken,
                    expiration as OffsetDateTime,
                    scopes,
                    "http://beta.harmonygen.animus.design/api/v1/HarmonyGen/SpotifyCallBack",
                    call.parameters["code"].toString()
            )
            obseverMetrics(Pair(counter, requestTimer))
            call.respond(RestResponse("Success"))
        } catch (e: Exception) {
            call.respond(RestError(e.toString(), "Failed to authorize spotify."))
        }
    }
}

/**
 * Add the spotify token to the database.
 *
 * @author Animus Null
 * @param harmonyGenCTX An instance of the JOOQ DSL Context.
 * @param userId A string representing the Spotify user ID from the API.
 * @param accessToken A string for Spotify Access Token.
 * @param refreshToken The token necessary for refreshing.
 * @param expiresIn The time the token expires, and needs to be refreshed.
 * @param scopes An array of the configured spotify Scopes.
 * @param redirectURI What URL was initially used for redirecting.
 * @param currentCode What code was last retrieved
 */
fun addSpotifyAccessToken(
        harmonyGenCTX: DSLContext,
        userId: String,
        accessToken: String,
        refreshToken: String,
        expiresIn: OffsetDateTime,
        scopes: Array<String>,
        redirectURI: String,
        currentCode: String) {
    val SPOTIFY_AUTH = animus.design.kharmonygendb.tables.SpotifyAuth.SPOTIFY_AUTH
    val data = SpotifyAuthRecord(
            userId, expiresIn, currentCode, redirectURI, "", accessToken, refreshToken, scopes)
    harmonyGenCTX.insertInto(SPOTIFY_AUTH)
            .values(data)
            .onDuplicateKeyUpdate()
            .set(SPOTIFY_AUTH.ID, userId)
            .execute()
}


/**
 * Get an instance of the spotify api.
 *
 * Reading the client credentials from a property file.
 * Connect to the spotify API.
 * @return An instance of the Spotify API.
 */
fun getSpotifyAPI(): Api {
    val spotifyProps = loadAppProperties("/spotify.properties")
    val api = Api.builder()
            .clientId(spotifyProps.getProperty("spotify.clientid"))
            .clientSecret(spotifyProps.getProperty("spotify.clientsecret"))
            .redirectURI("http://beta.harmonygen.animus.design/api/v1/HarmonyGen/SpotifyCallBack")
            .build()
    return api
}

/**
 * Build out the authorization URL for spotify. This will link
 * the spotify account with this program.
 *
 * @param clientID The spotify clientID pulled from a properties file.
 * @param clientSecret The spotify client secret pulled from a properties file.
 * @param scopes A space separated string of scopes.
 * @param state A unique identifier for the current state, too verify the auth request is correct.
 */
fun buildSpotifyURL(clientID: String, clientSecret: String, scopes: String, state: UUID): String {
    val api = getSpotifyAPI()
    val listOfScopes = scopes.split(" ")
    return api.createAuthorizeURL(listOfScopes, state.toString())
}
