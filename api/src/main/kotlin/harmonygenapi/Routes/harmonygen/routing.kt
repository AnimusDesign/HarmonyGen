package harmonygenapi.Routes.harmonygen

import io.ktor.routing.Routing
import org.jooq.DSLContext
import org.slf4j.LoggerFactory

val logger = LoggerFactory.getLogger("HarmonyGen.HarmonyGenAPI")

fun Routing.harmonyGenAPI(harmonygen_ctx: DSLContext, apiBaseUrl: String = "/api/v1") {
    val apiPath = "${apiBaseUrl}/HarmonyGen"
    logger.debug("Registering routes for HarmonyGen API")
    spotifyCallBack(harmonygen_ctx, apiPath)
    logger.debug("Finished registering HarmonyGen API routes")
}