package harmonygenapi.Routes.MusicBrainz

import animus.design.kmusicbrainzdb.tables.Artist
import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.util.toLocalDateTime
import org.jooq.DSLContext
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.util.*

fun Routing.getRecentArtistChanges(musicbrainz_ctx: DSLContext, apiBaseURL:String = "/api/v1/MusicBrainz") {
    val path = "$apiBaseURL/RecentChanges"
    val (counter, requestLatency) = buildMetricObservers(path, "MuiscBrainzGetRecentChanges")
    get(path) {
        val requestTimer = requestLatency.startTimer()
        logger.debug("Received input paramaters of: ${call.parameters}")
        val now = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
        now.add(Calendar.DAY_OF_MONTH, -1)
        logger.debug("Searching for recent edits greater than $now")
        val offset = OffsetDateTime.of(now.time.toLocalDateTime(), ZoneOffset.UTC)
        logger.debug("Normalizing timezone to UTC which results in: $offset")
        val recentChanges = musicbrainz_ctx.select()
                .from(Artist.ARTIST)
                .where(Artist.ARTIST.LAST_UPDATED.gt(offset))
                .orderBy(Artist.ARTIST.LAST_UPDATED.desc())
                .limit(100)
                .fetch()
        val rsp = mapOf<String, Any>(
                "RecentChanges" to recentChanges.map { it.intoMap() },
                "Page" to 1
                )
        obseverMetrics(Pair(counter, requestTimer))
        call.respond(rsp)
    }
}