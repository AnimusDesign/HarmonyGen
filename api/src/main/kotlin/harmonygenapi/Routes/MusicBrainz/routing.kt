package harmonygenapi.Routes.MusicBrainz

import io.ktor.routing.Routing
import org.jooq.DSLContext
import org.slf4j.LoggerFactory

val logger = LoggerFactory.getLogger("HarmonyGen.MusicBrainzAPI")

fun Routing.musicBrainzAPI(musicbrainz_ctx : DSLContext, apiBaseUrl:String = "/api/v1") {
    val apiPath = "${apiBaseUrl}/MusicBrainz"
    logger.debug("Registering routes for MusicBrainz API")
    getArtistById(musicbrainz_ctx, apiPath)
    getRecentArtistChanges(musicbrainz_ctx, apiPath)
    logger.debug("Finished registering MusicBrainz API routes")
}