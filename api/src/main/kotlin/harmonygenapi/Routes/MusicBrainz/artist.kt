package harmonygenapi.Routes.MusicBrainz

import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.prometheus.client.Counter
import io.prometheus.client.Summary
import org.jooq.DSLContext


val calculationsCounter = Counter.build()
        .name("my_library_calculations_total").help("Total calls.")
        .labelNames("key").register()


/**
 * Rest method to gather an artist by ID.
 *
 * Taking in an ID of either the type UUID or ID.
 * Query the MusicBrainz for the given artist identifier.
 *
 * The MusicBrainz has two database columns *gid* and *id*.
 * The gid is what is commonly seen through any third party service.
 * It is the UUID type. While *id* is an integer, and used largely
 * between internal joins.
 *
 * @param musicbrainz_ctx A pre built DSLContext for the MusicBrainz database.
 * @author [Animus Null](sean@animus.design)
 */
fun Routing.getArtistById(musicbrainz_ctx: DSLContext, apiBaseUrl: String = "/api/v1/MusicBrainz") {
    val path = "$apiBaseUrl/Artist/{id}"
    val (counter, requestLatency) = buildMetricObservers(path, "MuiscBrainzGetArtist")

    get(path) {
        val requestTimer = requestLatency.startTimer()
        val artistRecord = try {
            val id = java.util.UUID.fromString(call.parameters["id"])
            musicbrainz_ctx
                    .selectFrom(animus.design.kmusicbrainzdb.tables.Artist.ARTIST)
                    .where(animus.design.kmusicbrainzdb.tables.Artist.ARTIST.GID.eq(id))
                    .fetchOne()
        } catch (e: IllegalArgumentException) {
            kotlin.io.println(e)
            val id = call.parameters["id"]!!.toInt()
            musicbrainz_ctx
                    .selectFrom(animus.design.kmusicbrainzdb.tables.Artist.ARTIST)
                    .where(animus.design.kmusicbrainzdb.tables.Artist.ARTIST.ID.eq(id))
                    .fetchOne()
        }
        val artist = artistRecord.intoMap()
        artist["tags"] = "api/v1/MusicBrainz/ArtistTag/$artist"
        obseverMetrics(Pair(counter, requestTimer))
        call.respond(artist)
    }
}


/**
 * Build out base prometheus metric observers for counting and latency.
 *
 * @param path The api path, used in help. I.E. /api/v1/MusicBrainz/GetArtist
 * @param baseName The base name of metric, Latency and Count will be appended.
 * @return A pair of Counter and Summary
 */
fun buildMetricObservers(path: String, baseName: String): Pair<Counter, Summary> {
    val counter = Counter.build().name("${baseName}Count").help("Request count to $path.").register()
    val requestLatency = Summary.build()
            .name("${baseName}Latency").help("Request $path latency in seconds.").register()
    return Pair(counter, requestLatency)
}

/**
 * Increment metric counter, and stop latency timer.
 *
 * Given a pair of Counter, and Summary.Timer. Increment
 * the counter, and close the duration observation. This
 * should be used at the end of your HTTP request block.
 *
 * @author Animus Null
 * @param metrics A pair of Counter and Summary.Timer
 */
fun obseverMetrics(metrics: Pair<Counter, Summary.Timer>) {
    val (counter, requestTimer) = metrics
    counter.inc()
    requestTimer.observeDuration();
}